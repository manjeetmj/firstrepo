package com.springlogin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springlogin.Entity.User12;
import com.springlogin.repo.UserRepo;
@Service
public class Userimpl implements Userservice {

	@Autowired
	UserRepo userRepo;
	
	
	@Override
	public User12 saveUser(User12 user) {
		// TODO Auto-generated method stub
		return userRepo.save(user);
	}


	@Override
	public List<User12> loginUser(String email,String phone) {
		// TODO Auto-generated method stub
		return userRepo.findByEmailAndPhone(email , phone);
	}


	@Override
	public List<User12> loginUser12(String email, String password) {
		// TODO Auto-generated method stub
		return userRepo.findByEmailAndPassword(email, password);
	}

}
