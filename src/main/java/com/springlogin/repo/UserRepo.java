package com.springlogin.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springlogin.Entity.User12;

public interface UserRepo extends JpaRepository<User12, Integer> {

	List<User12> findByEmailAndPhone(String email,String phone);
	
	
	List<User12> findByEmailAndPassword(String email,String password);
	
	
	
}
